@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <a class="btn btn-outline-primary" href="{{route('news.edit', compact('news'))}}">Изменить</a>
            <div class="index mb-5 pb-5">
                <div class="text-center">
                    <h2>{{$news->name}}</h2>
                    <h4>автор: {{$news->user->name}}</h4>
                    <p>{{$news->text}}</p><br>
                    <p class="float-lg-start">дата создание {{$news->created_at}}</p>
                    <p class="float-end">дата публикации {{$news->publication_date}}</p>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <h2>Комментарии</h2>
            <form action="{{route('comments.store')}}" method="post">
                @csrf
                <input type="hidden" name="news_id" value="{{$news->id}}">
                <div class="form-group row">
                    <div class="col-md-12">
                                    <textarea class="form-control @error('text') is-invalid @enderror" id="text"
                                              rows="3" name="text">{{ old('text') }}</textarea>
                        @error('text')
                        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>
                <button class="btn btn-outline-primary">добавить комментарии</button>
            </form>
            @foreach($news->comments as $comment)
                @if($comment->verified == true)
                    <p>Автор: {{$comment->user->name}}<br>"{{$comment->text}}"</p>
                    <form action="{{action([\App\Http\Controllers\CommentController::class, 'destroy'], ['comment' => $comment])}}" method="post">
                        @csrf
                        @method('delete')
                        <button class="btn btn-outline-danger">удалить</button>
                    </form>
                @endif
                    <br>
            @endforeach
        </div>
    </div>

    <div class="row">
        <div class="col">
            <h2>Оценить</h2>

            <form action="{{action([\App\Http\Controllers\GradeController::class, 'store'])}}" method="post">
                @csrf
                <input type="hidden" name="user_id" value="{{\Illuminate\Support\Facades\Auth::user()->getAuthIdentifier()}}">
                <input type="hidden" name="news_id" value="{{$news->id}}">
                <select class="custom-select" name="qualitatively">
                    <option value="{{1}}">качественно</option>
                    <option value="{{-1}}">не качественно</option>
                </select>
                <select class="custom-select" name="actual">
                    <option value="{{1}}">актуально</option>
                    <option value="{{-1}}">не актуально</option>
                </select>
                <select class="custom-select" name="satisfied">
                    <option value="{{1}}">довольный</option>
                    <option value="{{-1}}">не довольный</option>
                </select>
                <button class="btn btn-outline-primary">оценить</button>
            </form>

            @foreach($news->grades as $grade)
                <form action="{{action([\App\Http\Controllers\GradeController::class, 'update'], ['grade' => $grade])}}" method="post">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="user_id" value="{{\Illuminate\Support\Facades\Auth::user()->getAuthIdentifier()}}">
                    <input type="hidden" name="news_id" value="{{$news->id}}">
                    <select class="custom-select" name="qualitatively">
                        <option value="{{1}}">качественно</option>
                        <option value="{{-1}}">не качественно</option>
                    </select>
                    <select class="custom-select" name="actual">
                        <option value="{{1}}">актуально</option>
                        <option value="{{-1}}">не актуально</option>
                    </select>
                    <select class="custom-select" name="satisfied">
                        <option value="{{1}}">довольный</option>
                        <option value="{{-1}}">не довольный</option>
                    </select>
                    <button class="btn btn-outline-primary">изменить оценку</button>
                </form>

                    <h5>автор: {{$grade->user->name}}</h5>
                    <li> @if($grade->qualitatively == 1) качественно @elseif($grade->qualitatively == -1) не качественно @endif</li>
                    <li> @if($grade->actual == 1) актуально @elseif($grade->actual == -1) не актуально @endif</li>
                    <li> @if($grade->satisfied == 1) довольный @elseif($grade->satisfied == -1) не довольный @endif</li>
                    <form action="{{action([\App\Http\Controllers\GradeController::class, 'destroy'], ['grade' => $grade])}}" method="post">
                        @csrf
                        @method('delete')
                        <button class="btn btn-outline-danger">
                            удалить
                        </button>
                    </form>
                    <br><br>
                @endforeach
            </div>
        </div>

@endsection
