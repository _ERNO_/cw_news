@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col">
            <h1 class="text-center">Последние Новости</h1>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <a class="btn btn-outline-primary" href="{{route('news.create')}}">Добавить новость</a>
            <div class="form">
                <form action="{{route('news.index')}}" class="float-end" method="GET">
                    <lable>Категории</lable>
                    <select name="category_id" class="custom-select">
                        <option value="{{null}}" selected>Все...</option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}" >{{$category->name}}</option>
                        @endforeach
                    </select>
                    <lable>Тэги</lable>
                    <select  name="tag_id" class="custom-select">
                        <option value="{{null}}" selected>Все...</option>
                        @foreach($tags as $tag)
                            <option value="{{$tag->id}}" >{{$tag->name}}</option>
                        @endforeach
                    </select>
                    <button class="btn btn-outline-primary">
                        фильтр
                    </button>
                </form>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col" >
            @foreach($news as $new)
                <form action="{{route('news.destroy', ['news' => $new])}}" method="post">
                    @csrf
                    @method('delete')
                    <button class="float-end btn btn-outline-danger">Удалить</button>
                </form>
                <div class="index mb-5 pb-5">
                <div class="text-center">
                    <h2><a class="text-decoration-none" href="{{route('news.show', ['news' => $new])}}">{{$new->name}}</a></h2>
                    <h4>автор: {{$new->user->name}}</h4>
                   <p>{{$new->text}}</p><br>
                </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="row">
        <div class="col">
            {{$news->links('pagination::bootstrap-4')}}
        </div>
    </div>
@endsection
