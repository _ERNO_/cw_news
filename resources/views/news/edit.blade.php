@extends('layouts.app')

@section('content')
    <h2 class="text-center">Изменить новость</h2>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <form action="{{route('news.update', ['news' => $news])}}" method="post">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="user_id" value="{{\Illuminate\Support\Facades\Auth::user()->getAuthIdentifier()}}">

                    <div class="form-group row">
                        <label for="institution_id" class="col-md-2 col-form-label text-md-right">Категория</label>
                        <div class="col-md-6">
                            <select class="custom-select" name="category_id">
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="institution_id" class="col-md-2 col-form-label text-md-right">Тэг</label>
                        <div class="col-md-6">
                            <select class="custom-select" name="tag_id">
                                @foreach($tags as $tag)
                                    <option value="{{$tag->id}}">{{$tag->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="user_id" class="col-md-2 col-form-label text-md-right">Название</label>
                        <div class="col-md-6">
                            <input type="text" name="name" class="w-100" value="{{$news->name}}" >
                            @error('name')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                    </div>


                    <div class="form-group row">
                        <label for="text"
                               class="col-md-2 col-form-label text-md-right">Содержимое</label>

                        <div class="col-md-6">
                                    <textarea class="form-control @error('text') is-invalid @enderror" id="text"
                                              rows="15"  name="text">{{$news->text}}</textarea>
                            @error('text')
                            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                    </div>


                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-6">
                            <button type="submit" class="btn btn-primary">
                                Изменить
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
