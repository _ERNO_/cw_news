<?php

namespace Database\Seeders;

use App\Models\Grade;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory()
            ->count(5)
            ->create();
        $this->call(CategorySeeder::class);
        $this->call(TagSeeder::class);
        $this->call(NewsSeeder::class);
        $this->call(GradeSeeder::class);
        $this->call(CommentSeeder::class);
    }
}
