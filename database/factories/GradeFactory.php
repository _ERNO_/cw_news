<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class GradeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'qualitatively' => rand(-1, 1),
            'actual' => rand(-1, 1),
            'satisfied' => rand(-1, 1),
            'news_id' => rand(1, 15),
            'user_id' => $this->faker->unique()->randomElement([1,2,3,4,5])
        ];
    }
}
