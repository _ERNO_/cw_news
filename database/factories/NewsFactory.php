<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class NewsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'text' => $this->faker->paragraph(15, 25),
            'name' => $this->faker->word(),
            'user_id' => rand(1, 5),
            'category_id' => rand(1, 5),
            'tag_id' => rand(1, 5),
        ];
    }
}
