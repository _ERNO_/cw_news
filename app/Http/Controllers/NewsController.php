<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\News;
use App\Models\Tag;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NewsController extends Controller
{

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        $newsQuery = News::query();

        if ($request->input('category_id')){
            $newsQuery->where('category_id', $request->category_id);
        }
        if ($request->input('tag_id')){
            $newsQuery->where('tag_id',  $request->tag_id);
        }

        $news = $newsQuery
            ->where('publication_date', '!=', null )
            ->where('publication_date', '<=', Carbon::today())->paginate(4);

        $categories = Category::all();
        $tags = Tag::all();
        return view('news.index', compact('news', 'categories', 'tags'));
    }


    /**
     * @return Application|Factory|View
     */
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('news.create', compact('categories', 'tags'));
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $news = new News($request->all());
        $news->save();
        return redirect()->route('news.index')->with('successfully created');
    }


    /**
     * @param News $news
     * @return Application|Factory|View
     */
    public function show(News $news)
    {
        return view('news.show', compact('news'));
    }


    /**
     * @param News $news
     * @return Application|Factory|View
     */
    public function edit(News $news)
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('news.edit', compact('news', 'categories', 'tags'));
    }


    /**
     * @param Request $request
     * @param News $news
     * @return RedirectResponse
     */
    public function update(Request $request, News $news): RedirectResponse
    {
        $news->update($request->all());
        return redirect()->route('news.index')->with('successfully updated');
    }


    /**
     * @param News $news
     * @return RedirectResponse
     */
    public function destroy(News $news): RedirectResponse
    {
        $news->delete();
        return redirect()->route('news.index')->with('successfully deleted');
    }
}
