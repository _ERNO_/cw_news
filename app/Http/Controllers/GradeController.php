<?php

namespace App\Http\Controllers;

use App\Models\Grade;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GradeController extends Controller
{





    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $grade = new Grade($request->all());
        $grade->user_id = Auth::user()->getAuthIdentifier();
        $grade->save();
        return back()->with('успешно сохранено');
    }






    /**
     * @param Request $request
     * @param Grade $grade
     * @return RedirectResponse
     */
    public function update(Request $request, Grade $grade): RedirectResponse
    {
        $grade->update($request->all());
        return back()->with('успешно сохранено');
    }


    /**
     * @param Grade $grade
     * @return RedirectResponse
     */
    public function destroy(Grade $grade): RedirectResponse
    {
        $grade->delete();
        return back()->with('успешно удалено');
    }
}
