<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{


    /**
     * @param User $user
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(User $user)
    {
        $Ar = $user->news->where('publication_date', '!=', null )
            ->where('publication_date', '<=', Carbon::today());
        $ArCount = count($Ar);
        $Q = 0;
        $A = 0;
        foreach ($user->grades as $grade)
        {
            $Q += $grade->qualitatively;
            $A += $grade->actual;
        }
        $result = ($ArCount + $Q + $A)/3;

        return view('users.show', compact('user', 'result'));
    }

}
