<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $comment = new Comment($request->all());
        $comment->user_id = Auth::user()->getAuthIdentifier();
        $comment->save();
        return back()->with('вы успешно добавили комментарии');
    }



    /**
     * @param Comment $comment
     * @return RedirectResponse
     */
    public function destroy(Comment $comment): RedirectResponse
    {
        $comment->delete();
        return back()->with('вы успешно удалили комментарии');
    }
}
